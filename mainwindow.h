#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "funcs.h"
#include "qcustomplot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void updateTextBrowserResult(QString s);
    void updateTextBrowserY(QString s);
    void updateGraphGist();
    void updateGraphcRaspredel();
    void updateUI();

private:
    void start();
    void init();

    funcs *randomizer = new funcs();

    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
