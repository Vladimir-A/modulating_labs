#ifndef FUNCS_H
#define FUNCS_H

#include <QThread>
#include <QVector>
#include <math.h>

#include "random.h"

class funcs : public QThread
{

    Q_OBJECT

public:
    funcs(QObject *parent = nullptr);

    void run();

    void gen_type_1_12();

    void gen_type_1_12(int value);


    //WARNING this function works only with (inervals = 20)
    QVector<double> GetFrequency(int intervals);

    QVector<double> GetFuncRaspr();
    QString GetInfo();

signals:
    void StringToTextBrowserForResult(QString);
    void StringToTextBrowserY(QString);
    void EndOfCalc();

private:
    QVector<int> randomazer(int max);
    void threadFor_1_12();
    void fGraphGist();

    QVector<int> randomValueFirst;
    QVector<double> randomValueEnd;
    QVector<double> frequency;
    QVector<double> FuncRaspr;
    double MatOj = 0;
    double MatOjTwo = 0;
    double MatOjThree = 0;
    double Disper = 0;

    unsigned int ch = 0;
    const int MAX = 500;

    //crutch for initialization first value
    int crutch = 0;

};

#endif // FUNCS_H
