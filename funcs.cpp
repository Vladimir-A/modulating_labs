﻿#include "funcs.h"

#include <iostream>

funcs::funcs(QObject *parent) : QThread(parent)
{

}

void funcs::run()
{
    switch (ch) {
        case 1:
            threadFor_1_12();
            ch = 0;
            break;

        default:
            break;
    }

}

void funcs::gen_type_1_12()
{
    ch = 1;
    //this->start();
//    run();
//    ch = 0;
}

void funcs::gen_type_1_12(int value)
{
    crutch = value;
    this->gen_type_1_12();
}

QVector<double> funcs::GetFrequency(int intervals)
{
    QVector<double> y3(intervals);

    if (intervals == 20){
        for (int i=0; i <MAX; i++){
            if (randomValueEnd[i] <= 0.05) y3[0]++;
            if (randomValueEnd[i] <= 0.10 and randomValueEnd[i] > 0.05) y3[1]++;
            if (randomValueEnd[i] <= 0.15 and randomValueEnd[i] > 0.10) y3[2]++;
            if (randomValueEnd[i] <= 0.20 and randomValueEnd[i] > 0.15) y3[3]++;
            if (randomValueEnd[i] <= 0.25 and randomValueEnd[i] > 0.20) y3[4]++;
            if (randomValueEnd[i] <= 0.30 and randomValueEnd[i] > 0.25) y3[5]++;
            if (randomValueEnd[i] <= 0.35 and randomValueEnd[i] > 0.30) y3[6]++;
            if (randomValueEnd[i] <= 0.40 and randomValueEnd[i] > 0.35) y3[7]++;
            if (randomValueEnd[i] <= 0.45 and randomValueEnd[i] > 0.40) y3[8]++;
            if (randomValueEnd[i] <= 0.50 and randomValueEnd[i] > 0.45) y3[9]++;
            if (randomValueEnd[i] <= 0.55 and randomValueEnd[i] > 0.50) y3[10]++;
            if (randomValueEnd[i] <= 0.60 and randomValueEnd[i] > 0.55) y3[11]++;
            if (randomValueEnd[i] <= 0.65 and randomValueEnd[i] > 0.60) y3[12]++;
            if (randomValueEnd[i] <= 0.70 and randomValueEnd[i] > 0.65) y3[13]++;
            if (randomValueEnd[i] <= 0.75 and randomValueEnd[i] > 0.70) y3[14]++;
            if (randomValueEnd[i] <= 0.80 and randomValueEnd[i] > 0.75) y3[15]++;
            if (randomValueEnd[i] <= 0.85 and randomValueEnd[i] > 0.80) y3[16]++;
            if (randomValueEnd[i] <= 0.90 and randomValueEnd[i] > 0.85) y3[17]++;
            if (randomValueEnd[i] <= 0.95 and randomValueEnd[i] > 0.90) y3[18]++;
            if (randomValueEnd[i] <= 1.00 and randomValueEnd[i] > 0.95) y3[19]++;
        }

    } else {
        throw "Broken function intervals not a 20";
    }

    frequency = y3;

    return frequency;
}

QVector<double> funcs::GetFuncRaspr()
{
    QVector<double> _buf(20);

    for (int i=1; i<20; ++i)
    {
        _buf[i] = _buf[i-1] + frequency[i];
    }

    FuncRaspr = _buf;

    return FuncRaspr;
}

QString funcs::GetInfo()
{
    MatOj = 0;
    MatOjTwo = 0;
    MatOjThree = 0;
    for (int i = 0; i < MAX; ++i) {
        MatOj += randomValueEnd[i] * 0.002;
        MatOjTwo += (randomValueEnd[i] * randomValueEnd[i]) * 0.002;
        MatOjThree += (randomValueEnd[i] * randomValueEnd[i] * randomValueEnd[i]) * 0.002;
    }

    Disper = MatOjTwo - (MatOj*MatOj);

    QString strToForm;
    strToForm.push_back("Математическое ожидание - " + QString::number(MatOj) + "\n");
    strToForm.push_back("Второй момент - " + QString::number(MatOjTwo) + "\n");
    strToForm.push_back("Третий момент - " + QString::number(MatOjThree) + "\n");
    strToForm.push_back("Дисперсия - " + QString::number(Disper));

    return strToForm;
}

QVector<int> funcs::randomazer(int max)
{
    QVector<int> buf;

    for (int i = 0; i < max; ++i) {
        buf.push_back(Random::get(1, 10000));
    }

    //Для вывода в форму
    QString strToForm;
    int i = 0;
    for (; i < (max-1); i++) {
        strToForm.push_back(QString::number(i+1) + " - " + QString::number(buf[i]) + "\n");
    }
    strToForm.push_back(QString::number(i+1) + " - " + QString::number(buf[i]));

    StringToTextBrowserY(strToForm);

    return buf;
}

void funcs::threadFor_1_12()
{
    //It was grate mistake - shit code
    if (crutch == 0){
        randomValueFirst = randomazer(1);
    } else {
        randomValueFirst.clear();
        randomValueFirst.push_back(crutch);
        crutch = 0;
        QString strToForm("1 - " +  QString::number(randomValueFirst[0]));
        StringToTextBrowserY(strToForm);
    }

    randomValueEnd.clear();

    const QVector<int> _A = {171, 172, 170};
    const QVector<int> _B = {177, 176, 178};
    const QVector<int> _C = {2, 35, 63};
    const QVector<int> _D = {30269, 30307, 30323};

    const int K = 3;

    QVector<float> x;
    x.resize(K);

    //take furst value
    QVector<int> _Y = {randomValueFirst[0]};

    for (int j = 0; j < MAX ; j++){

        QVector<int> yN;
        yN.resize(K);
        float s = 0;

        for(int i = 0 ; i < K; ++i){
            yN[i] = static_cast<int>(fabs(_A[i] * (_Y[i] % _B[i])-
                         _C[i] * static_cast<float>(_Y[i]) / _B[i]));
            _Y.push_back(yN[i]);
            x[i] = static_cast<float>(static_cast<float>(yN[i]) / static_cast<float>(_D[i]));
            s += x[i];
        }

        float _s;
        modf(s, &_s);
        randomValueEnd.push_back(fabs(s -_s));

        //For new rnd value
        int next = static_cast<int>(fabs(s -_s) * 100000);

        //take next value
        _Y = {next};
    }

    //Для вывода в форму
    QString strToForm;
    int i = 0;
    for (; i < (MAX-1); i++) {
        strToForm.push_back(QString::number(i+1) + " - " + QString::number(randomValueEnd[i]) + "\n");
    }
    strToForm.push_back(QString::number(i+1) + " - " + QString::number(randomValueEnd[i]));

    StringToTextBrowserForResult(strToForm);

    //signal End Calculate
    EndOfCalc();
}

void funcs::fGraphGist()
{

}
