#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    init();
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete randomizer;
    delete ui;
}

void MainWindow::start()
{
//    int _Y = 0;

//    if (ui->YForm->text() != " " ){
//        _Y = ui->YForm->text().toInt();
//    }
    if (ui->textBrowserForY->toPlainText() == QString()) {
        randomizer->gen_type_1_12();
        randomizer->start();
    } else {
        bool chek = true;
        randomizer->gen_type_1_12(ui->textBrowserForY->toPlainText().toInt(&chek,10));
        randomizer->start();
    }

}

void MainWindow::init()
{
    connect(randomizer,SIGNAL(StringToTextBrowserForResult(QString)),this,SLOT(updateTextBrowserResult(QString)));
    connect(randomizer,SIGNAL(StringToTextBrowserY(QString)),this,SLOT(updateTextBrowserY(QString)));
    connect(randomizer,SIGNAL(EndOfCalc()),this,SLOT(updateUI()));
}

void MainWindow::on_pushButton_clicked()
{
    start();
}

void MainWindow::updateTextBrowserResult(QString s)
{
    ui->textBrowserForEndResult->setText(s);
}

void MainWindow::updateTextBrowserY(QString s)
{
    ui->textBrowserForY->setText(s);
}

void MainWindow::updateGraphGist()
{
    QCustomPlot *customPlot = ui->GraphGist;

    ui->GraphGist->clearGraphs();
    ui->GraphGist->clearItems();
    ui->GraphGist->clearPlottables();
    ui->GraphGist->clearMask();
    ui->GraphGist->clearFocus();

    // prepare data:
//    QVector<double> x1(20), y1(20);
//    QVector<double> x2(100), y2(100);
    QVector<double> x3(20), y3(20);
//    QVector<double> x4(20), y4(20);
//    for (int i=0; i<x1.size(); ++i)
//    {
//      x1[i] = i/(double)(x1.size()-1)*10;
//      y1[i] = qCos(x1[i]*0.8+qSin(x1[i]*0.16+1.0))*qSin(x1[i]*0.54)+1.4;
//    }
//    for (int i=0; i<x2.size(); ++i)
//    {
//      x2[i] = i/(double)(x2.size()-1)*10;
//      y2[i] = qCos(x2[i]*0.85+qSin(x2[i]*0.165+1.1))*qSin(x2[i]*0.50)+1.7;
//    }
//    for (int i=0; i<20; ++i)
//        {
//          double _buf = 0;
//          for (int j = 0; j<25; j++){
//              _buf +=rndvalues[i*25+j];
//          }
//          _buf /=25;
//          y3[i] = _buf;
//        }

    y3 = randomizer->GetFrequency(20);

//    for (int i=0; i<x3.size(); ++i)
//    {
//      x3[i] = i/(double)(x3.size()-1)*10;
//    }
    for (int i=0; i<20; ++i)
    {
        x3[i] = i;
    }
//    for (int i=0; i<x4.size(); ++i)
//    {
//      x4[i] = x3[i];
//      y4[i] = (0.5-y3[i])+((x4[i]-2)*(x4[i]-2)*0.02);
//    }

    // create and configure plottables:
    QCPGraph *graph1 = customPlot->addGraph();
//    graph1->setData(x1, y1);
//    graph1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
//    graph1->setPen(QPen(QColor(120, 120, 120), 2));

//    QCPGraph *graph2 = customPlot->addGraph();
//    graph2->setData(x2, y2);
//    graph2->setPen(Qt::NoPen);
//    graph2->setBrush(QColor(200, 200, 200, 20));
//    graph2->setChannelFillGraph(graph1);

    QCPBars *bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setWidth(9/(double)x3.size());
    bars1->setData(x3, y3);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(10, 140, 70, 160));

//    QCPBars *bars2 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
//    bars2->setWidth(9/(double)x4.size());
//    bars2->setData(x4, y4);
//    bars2->setPen(Qt::NoPen);
//    bars2->setBrush(QColor(10, 100, 50, 70));
//    bars2->moveAbove(bars1);

    // move bars above graphs and grid below bars:
    customPlot->addLayer("abovemain", customPlot->layer("main"), QCustomPlot::limAbove);
    customPlot->addLayer("belowmain", customPlot->layer("main"), QCustomPlot::limBelow);
    graph1->setLayer("abovemain");
    customPlot->xAxis->grid()->setLayer("belowmain");
    customPlot->yAxis->grid()->setLayer("belowmain");

    // set some pens, brushes and backgrounds:
    customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    customPlot->setBackground(plotGradient);
    QLinearGradient axisRectGradient;
    axisRectGradient.setStart(0, 0);
    axisRectGradient.setFinalStop(0, 350);
    axisRectGradient.setColorAt(0, QColor(80, 80, 80));
    axisRectGradient.setColorAt(1, QColor(30, 30, 30));
    customPlot->axisRect()->setBackground(axisRectGradient);

    customPlot->rescaleAxes();
    customPlot->yAxis->setRange(0, 100);

    customPlot->xAxis->setLabelColor(Qt::white);
    customPlot->yAxis->setLabelColor(Qt::white);
    customPlot->xAxis->setLabel("Intervals");
    customPlot->yAxis->setLabel("frequency");

    ui->GraphGist->replot();
}

void MainWindow::updateGraphcRaspredel()
{
    QCustomPlot *customPlot = ui->GraphicRaspredel;

    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    customPlot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue
    customPlot->addGraph();
    //customPlot->graph(1)->setPen(QPen(Qt::red)); // line color red for second graph
    // generate some points of data (y0 for first, y1 for second graph):
    QVector<double> x(20), y0(20);//, y1(251);
//    for (int i=0; i<251; ++i)
//    {
//      //x[i] = i;
//      y0[i] = qExp(-i/150.0)*qCos(i/10.0); // exponentially decaying cosine
//      //y1[i] = qExp(-i/150.0);              // exponential envelope
//    }

    for (int i=0; i<20; ++i)
    {
        x[i] = i;
    }

    y0 = randomizer->GetFuncRaspr();

    // configure right and top axis to show ticks but no labels:
    // (see QCPAxisRect::setupFullAxesBox for a quicker method to do this)
    customPlot->xAxis2->setVisible(true);
    customPlot->xAxis2->setTickLabels(false);
    customPlot->yAxis2->setVisible(true);
    customPlot->yAxis2->setTickLabels(false);
    // make left and bottom axes always transfer their ranges to right and top axes:
    connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));
    // pass data points to graphs:
    customPlot->graph(0)->setData(x, y0);
    //customPlot->graph(1)->setData(x, y1);
    // let the ranges scale themselves so graph 0 fits perfectly in the visible area:
    customPlot->graph(0)->rescaleAxes();
    // same thing for graph 1, but only enlarge ranges (in case graph 1 is smaller than graph 0):
    //customPlot->graph(1)->rescaleAxes(true);
    // Note: we could have also just called customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    customPlot->xAxis->setLabel("Intervals");
    customPlot->yAxis->setLabel("f(x)");

    ui->GraphicRaspredel->replot();
}

void MainWindow::updateUI()
{
    updateGraphGist();
    updateGraphcRaspredel();
    ui->textBrowserForInfo->setText(randomizer->GetInfo());
}
